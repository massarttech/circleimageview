# CircleImageView [![](https://jitpack.io/v/com.gitlab.massarttech/circleimageview.svg)](https://jitpack.io/#com.gitlab.massarttech/circleimageview)
**A fast circular ImageView perfect for profile images.**

<img src="https://gitlab.com/massarttech/circleimageview/raw/master/screenshot.png?inline=false" width="300" height="500"/>

### Setup
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:
```
dependencies {
    implementation 'com.gitlab.massarttech:circleimageview:1.0.0'
}
```

### Usage
```xml
 <com.massarttech.android.circleimageview.CircleImageView
            android:layout_width="160dp"
            android:layout_height="160dp"
            android:layout_centerInParent="true"
            android:src="@drawable/taylor"
            app:civBorderWidth="2dp"
            app:civBorderColor="@color/dark" />
```

****Original repo https://github.com/hdodenhof/CircleImageView****